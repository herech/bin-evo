/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CellularAutomaton.cpp
 * Author: jan
 * 
 * Created on 1. března 2017, 0:32
 */

#include "CellularAutomaton.hpp"
#include <iostream>

using namespace std;

    CellularAutomaton2DGrid CellularAutomaton::space2D;  
    CellularAutomaton2DGrid CellularAutomaton::newComputedSpace2D; 

    CellularAutomaton2DGrid* CellularAutomaton::space2D_p;    
    CellularAutomaton2DGrid* CellularAutomaton::newComputedSpace2D_p; 
    int CellularAutomaton::lastSixValuesOfCorner[6] = {0,0,0,0,0,0};

CellularAutomaton::CellularAutomaton() {
}

CellularAutomaton::CellularAutomaton(const CellularAutomaton& orig) {
}

CellularAutomaton::~CellularAutomaton() {
}

CellularAutomaton2DGrid** CellularAutomaton::get2DSpace() {
    return &space2D_p;
}

void CellularAutomaton::init() {
    space2D_p = &space2D;
    newComputedSpace2D_p = &newComputedSpace2D;
    
    for (int row = 0; row < WIDTH_OF_SPACE;row++) {
        for (int col = 0; col < HEIGHT_OF_SPACE;col++) {
            space2D[row][col] = 0;
        }
    }
    
    space2D[HEIGHT_OF_SPACE / 2][WIDTH_OF_SPACE / 2] = 7; // do středu umístíme barevnou buňku
    //space2D[HEIGHT_OF_SPACE / 2 - 1][WIDTH_OF_SPACE / 2 - 1] = 7; // do středu umístíme barevnou buňku
    //space2D[HEIGHT_OF_SPACE / 2 + 1][WIDTH_OF_SPACE / 2] = 8; // do středu umístíme barevnou buňku
    //space2D[HEIGHT_OF_SPACE / 2 + 2][WIDTH_OF_SPACE / 2 - 2] = 7; // do středu umístíme barevnou buňku
}

void CellularAutomaton::printValuesIn2DSpace() {
    for (int row = 0; row < WIDTH_OF_SPACE;row++) {
        cout << endl;
        for (int col = 0; col < HEIGHT_OF_SPACE;col++) {
            cout << (*space2D_p)[row][col];
        }
    }
}

void CellularAutomaton::calcNewValuesInSpace(unsigned int* transitionFunction, int numberOfTransitions) {
    for (int row = 0; row < HEIGHT_OF_SPACE;row++) {
        for (int col = 0; col < WIDTH_OF_SPACE;col++) {
            int leftValue = (*space2D_p)[row][(col == 0) ? WIDTH_OF_SPACE - 1 : col - 1];
            int rightValue = (*space2D_p)[row][(col == WIDTH_OF_SPACE - 1) ? 0 : col + 1];
            int topValue = (*space2D_p)[(row == 0) ? HEIGHT_OF_SPACE - 1 : row - 1][col];
            int bottomValue = (*space2D_p)[(row == HEIGHT_OF_SPACE - 1) ? 0 : row + 1][col];
            int centerValue = (*space2D_p)[row][col];
            
            int computedValue = centerValue;
            int sum = leftValue + rightValue + topValue + bottomValue + centerValue;
            
            for (int i = 0; i < numberOfTransitions * 2; i += 2) {
                if (transitionFunction[i] == sum) {
                    
                    computedValue = transitionFunction[i + 1];
                    //cout << leftValue << " " << rightValue << " " << topValue << " " << bottomValue << " " << centerValue << " " << computedValue << endl;
                }
                
                (*newComputedSpace2D_p)[row][col] = computedValue;
            }
            
            /*for (int i = 0; i < numberOfTransitions * 6; i += 6) {
                if (transitionFunction[i] == topValue && 
                    transitionFunction[i + 1] == bottomValue &&
                    transitionFunction[i + 2] == leftValue &&
                    transitionFunction[i + 3] == rightValue &&
                    transitionFunction[i + 4] == centerValue) {
                    
                    computedValue = transitionFunction[i + 5];
                }
                
                (*newComputedSpace2D_p)[row][col] = computedValue;
            }*/
        }
    }
    
    // swap pointerů
    CellularAutomaton2DGrid *tmpSpace2D_p = space2D_p;
    space2D_p = newComputedSpace2D_p; // swap pointerů
    newComputedSpace2D_p = tmpSpace2D_p;
    
    // nastaveni poslednich sesti hodnot rohové bunky
    lastSixValuesOfCorner[0] = lastSixValuesOfCorner[1];
    lastSixValuesOfCorner[1] = lastSixValuesOfCorner[2];
    lastSixValuesOfCorner[2] = lastSixValuesOfCorner[3];
    lastSixValuesOfCorner[3] = lastSixValuesOfCorner[4];
    lastSixValuesOfCorner[4] = lastSixValuesOfCorner[5];
    lastSixValuesOfCorner[5] = (*space2D_p)[0][0];
}


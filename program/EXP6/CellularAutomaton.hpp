/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CellularAutomaton.hpp
 * Author: jan
 *
 * Created on 1. března 2017, 0:32
 */

#ifndef CELLULARAUTOMATON_HPP
#define CELLULARAUTOMATON_HPP
#define WIDTH_OF_SPACE 20
#define HEIGHT_OF_SPACE 20
#define NUMBER_OF_CELL_VALUES 13

using CellularAutomaton2DGrid = int[WIDTH_OF_SPACE][HEIGHT_OF_SPACE]; // 2D prostor = mřížka buněk celulárního automatu

class CellularAutomaton {
public:
    
    CellularAutomaton();
    CellularAutomaton(const CellularAutomaton& orig);
    virtual ~CellularAutomaton();
    
    static void calcNewValuesInSpace(unsigned int* transitionFunction, int numberOfTransitions);
    static void printValuesIn2DSpace();
    static void init();
    static CellularAutomaton2DGrid** get2DSpace();
    
    static int lastSixValuesOfCorner[6];
private:
    static CellularAutomaton2DGrid space2D;    // 2D prostor = mřížka buněk celulárního automatu, podle kterých se nejdříve počítají nové hodnoty buňek a následně 
    static CellularAutomaton2DGrid newComputedSpace2D; // 2D prostor = mřížka buněk celulárního automatu, které jsou nově vypočteny

    static CellularAutomaton2DGrid *space2D_p;    // 2D prostor = mřížka buněk celulárního automatu, podle kterých se nejdříve počítají nové hodnoty buňek a následně 
    static CellularAutomaton2DGrid *newComputedSpace2D_p; // 2D prostor = mřížka buněk celulárního automatu, které jsou nově vypočteny
};

#endif /* CELLULARAUTOMATON_HPP */


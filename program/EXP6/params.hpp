//////////////////////////////////////////////////////////////
// VZOROVA IMPLEMENTACE JEDNODUCHEHO GENETICKEHO ALGORITMU  //
// JAZYK C, PREKLAD S OHLEDEM NA NORMU C99                  //
// (c) MICHAL BIDLO, 2011                                   //
// UVEDENY KOD SMI BYT POUZIT VYHRADNE PRO POTREBY PROJEKTU //
// V KURZU BIOLOGII INSPIROVANE POCITACE NA FIT VUT V BRNE  //
//////////////////////////////////////////////////////////////

#ifndef PARAMS_H
#define PARAMS_H

// pro vypis prubehu evoluce na stdout, jinak zakomentujte
#define DEBUG

//----------------------- parametry genetickeho algoritmu ----------------------
// pravdepodobnost mutace
#define PMUT 15
// pocet mutovanych genu v chromozomu
#define MUTAGENES 3
// pravdepodobnost krizeni
#define PCROSS 70
// pocet jedincu v turnajove selekce
#define TOUR 8
// velikost populace
#define POPSIZE 40
// maximalni pocet generaci
#define GENERATIONS 300 //puvodne 1000
// delka chromozomu
//#define CHLEN 600 // 6 * 100 (100 pravidel zakodovanych na 6 integerů = 600 integerů)
//#define CHLEN 32 // 16 * 2 (16 pravidel zakodovanych na 2 integerů = 32 integerů)
//#define CHLEN 42 // 5 stavů buňky, tzn. suma od 0 do 4*5 (20) = 21 * 2
//#define CHLEN 52 // 6 stavů buňky, tzn. suma od 0 do 5*5 (25) = 26 * 2
//#define CHLEN 62 // 7 stavů buňky, tzn. suma od 0 do 6*5 (30) = 31 * 2
//#define CHLEN 82 // 9 stavů buňky, tzn. suma od 0 do 8*5 (40) = 41 * 2
#define CHLEN 142 // 13 stavů buňky, tzn. suma od 0 do 12*5 (70) = 71 * 2

typedef unsigned int UINT;
typedef int BOOL;

// definice typu chromozomu GA - UPRAVTE SI DLE POTREBY
typedef struct {
    UINT chromosome[CHLEN];  // vlastni napln chromozomu
    UINT fitness;   // fitness daneho jedince
    BOOL evaluate;  // zda je treba znovu vyhodnotit fitness
} GA_chromosome;

// prototypy funkci pro GA
UINT urandom(UINT low, UINT high);

void initialize(GA_chromosome *g);
void crossover(GA_chromosome *parent1, GA_chromosome *parent2, 
                GA_chromosome *child1, GA_chromosome *child2);
BOOL mutator(GA_chromosome *genome, UINT _pmut);
UINT fitness(GA_chromosome *genome);
void gprint(GA_chromosome *genome);
void evolve();
BOOL stop();

#endif

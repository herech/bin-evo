//////////////////////////////////////////////////////////////
// VZOROVA IMPLEMENTACE JEDNODUCHEHO GENETICKEHO ALGORITMU  //
// JAZYK C, PREKLAD S OHLEDEM NA NORMU C99                  //
// (c) MICHAL BIDLO, 2011                                   //
// UVEDENY KOD SMI BYT POUZIT VYHRADNE PRO POTREBY PROJEKTU //
// V KURZU BIOLOGII INSPIROVANE POCITACE NA FIT VUT V BRNE  //
//////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <fstream>

#include "params.hpp"
#include "CellularAutomaton.hpp"

using namespace std;

// jednotky pro specifikaci pravdepodobnosti genetickych operatoru:
// 100 - procenta, 1000 - promile. NENI TREBA MENIT
const UINT unit = 100;
// delka chromozomu - s touto promennou pracuje GA
const UINT glength = CHLEN;
// maximalni fitness - pocet bunek ktere predatvuji hledany vzor CA
const UINT max_fit = 17;

#ifdef DEBUG
const UINT generations = GENERATIONS; // 0 - pocet generaci neni pri ladeni omezen
#else
const UINT generations = GENERATIONS;   // po tomto poctu je GA zastaven
#endif

// !!!! nas GA pracuje pouze se sudymi pocty jedincu v populaci !!!!
UINT _popsize = (POPSIZE & 1) ? POPSIZE + 1 : POPSIZE;

// ------------------- implementace genetickeho algoritmu ----------------------
// *****************************************************************************

GA_chromosome best; // njelepsi dosud nalezene reseni
UINT best_ever; // fitness dosud nejlepsiho jedince

UINT generation;    // pocitadlo generaci
GA_chromosome *population;
GA_chromosome *next_population;
// pracovni populace - pouze sudy pocet jedincu !!!!
GA_chromosome pool1[(POPSIZE & 1) ? POPSIZE + 1 : POPSIZE];
GA_chromosome pool2[(POPSIZE & 1) ? POPSIZE + 1 : POPSIZE];

// evolucni cyklus SGA: nova populace o _popsize jedincich je tvorena krizenimi
// a mutaci turnajem vybranych jedincu predchazejici generace. 
void evolve()
{
    // inicializace promennych
    generation = 0;
    best.fitness = 0;
    best_ever = 0;
    GA_chromosome ind1_new, ind2_new;
    UINT _tour = (TOUR >= 2 ? TOUR : 2);
    UINT i1;
    
    // inicializace populace
    for (UINT i = 0; i < _popsize; i++)
    {
        initialize(&pool1[i]);
        pool1[i].evaluate = 1;
    }
    // evolucni cyklus
    do
    {
        generation++;
        if (generation % 100 == 0) {
            cout << "generation: " << generation << "/" << generations << endl << flush;
        }
        if (generation & 1)
        {
            population = pool1;
            next_population = pool2;
        }
        else
        {
            population = pool2;
            next_population = pool1;
        }
        // ohodnoceni populace
        for (UINT i = 0; i < _popsize; i++)
        {
            if (population[i].evaluate)
            {
                population[i].fitness = fitness(&population[i]);
                if (population[i].fitness >= best.fitness) {
                    best = population[i];
                                }
                population[i].evaluate = 0;
            }
        }
        // elitizmus
        next_population[0] = best;  // dosud nejlepsi nalezeny jedinec...
        GA_chromosome mutant = best; mutator(&mutant, unit);
        next_population[1] = mutant;    // ...a mutant nejlepsiho
        // tvorba nove populace
        for (UINT i = 2; i < _popsize; i += 2)
        {
            GA_chromosome *ind1 = NULL, *ind2 = NULL;
            // turnajovy vyber jedincu
            for (UINT t = 0; t < _tour; t++)
            {
                i1 = urandom(0, _popsize - 1);
                if (ind1 == NULL) ind1 = &population[i1];
                else if (ind2 == NULL) ind2 = &population[i1];
                else if (population[i1].fitness > ind1->fitness)
                    ind1 = &population[i1];
                else if (population[i1].fitness > ind2->fitness)
                    ind2 = &population[i1];
            }
            // krizeni nebudeme pouzivat
            /*if (urandom(0, unit) < PCROSS)
            {
                crossover(ind1, ind2, &ind1_new, &ind2_new);
                ind1_new.evaluate = 1;
                ind2_new.evaluate = 1;
            }
            else    // prechod jedincu bez krizeni
            {
                ind1_new = *ind1;
                ind2_new = *ind2;
            }*/
                        
            ind1_new = *ind1;
            ind2_new = *ind2;
                        
            // mutace
            if (mutator(&ind1_new, PMUT)) ind1_new.evaluate = 1;
            if (mutator(&ind2_new, PMUT)) ind2_new.evaluate = 1;
            // umisteni potomku do nove populace
            next_population[i] = ind1_new;
            next_population[i + 1] = ind2_new;
        }
    } while (!stop());
}

// *****************************************************************************
// --------------- geneticke operatory a podpurne funkce pro GA ----------------

// generuje cele cislo v rozsahu low-high vcetne
UINT urandom(UINT low, UINT high)
{
    return rand() % (high - low + 1) + low;
}

// vypis chromozomu - ZMENTE SI DLE POTREBY
void gprint(GA_chromosome *genome)
{
    for (UINT i = 0; i < glength; i++) {
        if (i % 2 != 0) {
            printf("%d", genome->chromosome[i]);
            putchar('\n');
        }
        else {
            printf("%d ", genome->chromosome[i]);
        }
    }
    putchar('\n');
}

// uložení genomu do souboru .tab jako přechodová funkce pro simulátor BiCAS.py
void createTabOutputFile(GA_chromosome *genome) {
    ofstream myfile;
    myfile.open ("res.tab");
    
    myfile << NUMBER_OF_CELL_VALUES << endl;
    
    for (UINT i = 0; i < glength; i++) {
        if (i % 2 != 0) {
            myfile << genome->chromosome[i] << endl;
        }
        else {
            myfile << genome->chromosome[i] << " ";
        }
    }
    myfile.close();
}

// uložení poč. stavu CA do souboru .cas pro simulátor BiCAS.py
void createCasOutputFile() {
    ofstream myfile;
    myfile.open ("res.cas");
    
    CellularAutomaton::init();
    CellularAutomaton2DGrid** cellularAutomaton2DGrid = CellularAutomaton::get2DSpace();
    
    for (int row = 0; row < WIDTH_OF_SPACE;row++) {
        for (int col = 0; col < HEIGHT_OF_SPACE;col++) {
            myfile << (**cellularAutomaton2DGrid)[row][col];
            if (col + 1 != HEIGHT_OF_SPACE) {
                myfile << " ";
            }
        }
        myfile << endl;
    }
    
    myfile.close();
}

// inicializace populace nahodne - ZMENTE SI DLE POTREBY
void initialize(GA_chromosome *genome)
{

    int j = 0;
    for (UINT i = 0; i < glength; i++) {
        if (i % 2 == 0) {
            genome->chromosome[i] = j++;
        }
        else {
            genome->chromosome[i] = urandom(0, NUMBER_OF_CELL_VALUES - 1);
        }
    }
}

// krizeni - ZMENTE SI DLE POTREBY
void crossover(GA_chromosome *parent1, GA_chromosome *parent2,
                GA_chromosome *child1, GA_chromosome *child2)
{
    // zde standardni jednobodove krizeni
    UINT cpoint = urandom(1, glength - 1);
    
    for (UINT i = 0; i < glength; i++)
    {
        if (i < cpoint)
        {
            child1->chromosome[i] = parent1->chromosome[i];
            child2->chromosome[i] = parent2->chromosome[i];
        }
        else
        {
            child1->chromosome[i] = parent2->chromosome[i];
            child2->chromosome[i] = parent1->chromosome[i];
        }
    }
}

// mutace - ZMENTE SI DLE POTREBY. je vsak treba zachovat navratovou hodnotu!
BOOL mutator(GA_chromosome *genome, UINT _pmut)
{
    
    for (UINT i = 0; i < glength - 1; i++) {
        // mutujeme jen hodnoty stavů ne sumy
        if (i % 2 != 0 && urandom(0, unit) <= _pmut) {
            UINT g = urandom(0, NUMBER_OF_CELL_VALUES - 1);
            genome->chromosome[i] = g;
        }
    }
    
    return 1; // s pravděpodobnosti hranici s jistotou jak by rekl klasik se neco zmutuje
    
}

// test na zastaveni evoluce - V PRIPADE POTREBY ZMENTE
BOOL stop()
{
    if (best.fitness > best_ever)
    {
        best_ever = best.fitness;
#ifdef DEBUG
        printf("Fitness = %d in generation %d\n", 
                        best_ever,      generation);
//        gprint(&best);
#endif
    }
        
    if (best_ever == max_fit || (generations > 0 && generation == generations))
    {
        if (best_ever == max_fit)
        {
            printf("success; generation=%d, fitness=%d/%d\n",
                                    generation, best_ever, max_fit);
            //gprint(&best);
        }
        else printf("failed; generation=%d, fitness=%d/%d\n",
                                    generation, best_ever, max_fit);
        return 1;
    }
    
    return 0;
}

// evaluace fitness pro zadaneho jedince - provedem 20 kroků CA a zjišťujeme běhm nich nejlépší dosaženou fitness
// funkci calcNewValuesInSpace předáváme jako přechodovou funkci genom jedince
UINT fitness(GA_chromosome *genome)
{
    UINT fit = 0;
    
    CellularAutomaton::init();
    CellularAutomaton2DGrid** cellularAutomaton2DGrid = CellularAutomaton::get2DSpace();

    UINT max_fit = 0;
     for (int i = 0; i < 20; i++) {
        CellularAutomaton::calcNewValuesInSpace(genome->chromosome, CHLEN / 2);
        fit = 0;
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 - 1][WIDTH_OF_SPACE / 2 - 1] == 11) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 - 2][WIDTH_OF_SPACE / 2 - 2] == 12) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 - 3][WIDTH_OF_SPACE / 2 - 3] == 11) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 - 4][WIDTH_OF_SPACE / 2 - 4] == 12) {
            fit++;
        }
        
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 + 1][WIDTH_OF_SPACE / 2 - 1] == 11) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 + 2][WIDTH_OF_SPACE / 2 - 2] == 12) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 + 3][WIDTH_OF_SPACE / 2 - 3] == 11) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 + 4][WIDTH_OF_SPACE / 2 - 4] == 12) {
            fit++;
        }
        
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 - 1][WIDTH_OF_SPACE / 2 + 1] == 11) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 - 2][WIDTH_OF_SPACE / 2 + 2] == 12) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 - 3][WIDTH_OF_SPACE / 2 + 3] == 11) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 - 4][WIDTH_OF_SPACE / 2 + 4] == 12) {
            fit++;
        }
        
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 + 1][WIDTH_OF_SPACE / 2 + 1] == 11) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 + 2][WIDTH_OF_SPACE / 2 + 2] == 12) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 + 3][WIDTH_OF_SPACE / 2 + 3] == 11) {
            fit++;
        }
        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2 + 4][WIDTH_OF_SPACE / 2 + 4] == 12) {
            fit++;
        }

        if ((**cellularAutomaton2DGrid)[HEIGHT_OF_SPACE / 2][WIDTH_OF_SPACE / 2] == 12) {
            fit++;
        }
        
                        
        // penalizujeme blikající automat
        if (CellularAutomaton::lastSixValuesOfCorner[0] == CellularAutomaton::lastSixValuesOfCorner[2] 
        && CellularAutomaton::lastSixValuesOfCorner[2] == CellularAutomaton::lastSixValuesOfCorner[4] 
        && CellularAutomaton::lastSixValuesOfCorner[0] != CellularAutomaton::lastSixValuesOfCorner[1]
        && CellularAutomaton::lastSixValuesOfCorner[1] == CellularAutomaton::lastSixValuesOfCorner[3]
        && CellularAutomaton::lastSixValuesOfCorner[3] == CellularAutomaton::lastSixValuesOfCorner[5]) {
            fit = 0;
        }
        
        if (CellularAutomaton::lastSixValuesOfCorner[0] == CellularAutomaton::lastSixValuesOfCorner[3] 
        && CellularAutomaton::lastSixValuesOfCorner[1] == CellularAutomaton::lastSixValuesOfCorner[4] 
        && CellularAutomaton::lastSixValuesOfCorner[2] == CellularAutomaton::lastSixValuesOfCorner[5]
        && CellularAutomaton::lastSixValuesOfCorner[0] != CellularAutomaton::lastSixValuesOfCorner[1]
        && CellularAutomaton::lastSixValuesOfCorner[0] != CellularAutomaton::lastSixValuesOfCorner[2]
        && CellularAutomaton::lastSixValuesOfCorner[1] != CellularAutomaton::lastSixValuesOfCorner[2]) {
            fit = 0;
        }
        
        
        if (fit > max_fit) {
            max_fit = fit;
        }

    }
            
    return max_fit;

}

// *****************************************************************************
// ------------------------------ hlavni program -------------------------------

int main(int argc, char *argv[])
{
    // podle toho jestli hledame nove reseni, nebo spoustime uz nalezene s konkretnim seedem, nastavímne seed
    #if RES == 0
    time_t seed = time(0);
    #elif RES == 1
    time_t seed = 1492551897;
    #elif RES == 2
    time_t seed = 1492622805;
    #endif

    cout << "srand: " << seed << endl;
    srand(seed); // random seed - NUTNE
    
    // spustime genetický algoritmus
    evolve();
    
    // vypiseme genom nalezeneho jedince
    //cout << "Transition function of best individual: " << endl;
    //gprint(&best);
    
    // uložíme si informace o to jaký byl seed a jaké fitness se dosáhlo
    ofstream myfile;
    myfile.open ("seed_and_fitness.txt");
    myfile << "SEED: " << seed << ", FITNESS: "<< best_ever << "/" << max_fit << endl;
    myfile.close();
    
    // vytvoříem soubory .cas a .tab pro simulátor BiCAS.py
    createTabOutputFile(&best);
    createCasOutputFile();

    return 0;
}
